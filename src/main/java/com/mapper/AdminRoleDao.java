package com.mapper;

import com.pojo.AdminRole;
import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/8 20:30
 * 描述：管理员角色数据访问借口
 */
public interface AdminRoleDao {
    /**
     * 查询所有管理员角色信息
     * @return 返回管理员信息列表
     */
    List<AdminRole> findAllAdminRole();

    /**
     * 查询一个角色有多个管理员
     * @return 返回管理员信息列表
     */
    List<AdminRole> findAdmin();
}
