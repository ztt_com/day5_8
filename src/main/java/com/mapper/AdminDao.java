package com.mapper;

import com.pojo.Admin;
import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/8 22:16
 * 描述：管理员数据访问接口
 */
public interface AdminDao {
    /**
     * 查询所有管理员
     * @return 返回管理员列表
     */
    List<Admin> findAll();

    /**
     * 查询一个管理员有多个角色
     * @return 角色列表
     */
    List<Admin> findAdminRole();
}
