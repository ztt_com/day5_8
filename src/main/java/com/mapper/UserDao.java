package com.mapper;

import com.pojo.QueryVo;
import com.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/8 15:52
 * 描述：针对用户数据访问接口
 */
public interface UserDao {
    /**
     * 查询所有用户
     * @return 返回用户列表
     */
    List<User> findAll();


    /**
     * 通过用户名和地址查询指定的用户
     * @param name 用户名
     * @param address 地址
     * @return
     */
    User findUserByCondition(@Param("userName") String name, @Param("userAddress")String address);

    /**
     * 更新用户
     * @param user 用户实体
     */
    void updateUser(User user) ;

    /**
     * 通过自定义实体查询用户列表
     * @param vo 自定义实体
     * @return 返回用户列表
     */
    List<User> findUserByQueryVo(QueryVo vo) ;

    /**
     * 通过用户名查询用户
     * @param username 用户名
     * @return 返回用户实体
     */
    User selectUserByUsername(String username) ;

}
