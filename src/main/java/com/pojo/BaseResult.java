package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 定义的响应实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResult {
    private Integer code ; //状态码 0,存在数据 1,不存在数据
    private String msg   ; //提示信息
    private Object data  ; //里面包含响应集合数据或者实体数据
    private Integer count ; //总记录数
}
