package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * mybatis映射框架,在书写实体类的时候,属性名称必须小写
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    /**
     *user表字段
     id int(11) NOT NULL
     username   varchar(10) NULL
     gender varchar(3) NULL
     age    int(11) NULL
     address    varchar(50) NULL
     */

    //如果实体类的属性和表的字段不对应
    private Integer userId ;
    private String  userName ;
    private String  userGender;
    private Integer userAge ;
    private String  userAddress ;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userGender='" + userGender + '\'' +
                ", userAge=" + userAge +
                ", userAddress='" + userAddress + '\'' +
                '}';
    }
}
