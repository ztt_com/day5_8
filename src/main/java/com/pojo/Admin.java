package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/8 20:18
 * 描述：管理员信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Admin {
    private Integer id;
    private String name;
    private String notes;

    //集合属性
    private List<AdminRole> adminRoles ;
    private AdminRole adminRole;

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
    //一个用户有多个账户

}
