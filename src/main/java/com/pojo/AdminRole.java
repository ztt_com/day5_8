package com.pojo;

/**
 * 作者：涛
 * 时间：2023/5/8 20:21
 * 描述：管理员的角色信息
 */
public class AdminRole {
    private Integer roleid;//管理员角色编号
    private String rolename;//管理员角色姓名
    private String rolegender;//管理员角色性别

    private Admin admin;

    public AdminRole() {
    }

    public AdminRole(Integer roleid, String rolename, String rolegender, Admin admin) {
        this.roleid = roleid;
        this.rolename = rolename;
        this.rolegender = rolegender;
        this.admin = admin;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getRolegender() {
        return rolegender;
    }

    public void setRolegender(String rolegender) {
        this.rolegender = rolegender;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "AdminRole{" +
                "roleid=" + roleid +
                ", rolename='" + rolename + '\'' +
                ", rolegender='" + rolegender + '\'' +
                ", admin=" + admin +
                '}';
    }
}
