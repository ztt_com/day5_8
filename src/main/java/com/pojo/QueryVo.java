package com.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 自定义实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryVo {
    private List<Integer> ids ; //list集合属性 属性名称ids

    public static void main(String[] args) {
        System.out.println("helloworld");
    }
}
