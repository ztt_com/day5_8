package com.service.impl;

import com.mapper.UserDao;
import com.pojo.BaseResult;
import com.pojo.User;
import com.service.UserService;
import com.utils.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

/**
 * 作者：涛
 * 时间：2023/5/8 17:45
 * 描述：针对用户接口的实现
 */
public class UserServiceImpl implements UserService {
    //声明UserDao类型 变量
    private UserDao userDao ;
    @Override
    public BaseResult checkUser(String username) {
        //调用dao层接口---mybatis实现
        SqlSession sqlSession = MyBatisUtil.openSession() ;
        userDao = sqlSession.getMapper(UserDao.class);

        User user = userDao.selectUserByUsername(username);
        //封装BaseResult对象
        BaseResult result ;
        if(user!=null){
            //存在,找到了
            return   result = new BaseResult(0,"用户名太受欢迎,请更换",null,null) ;
        }


        return  result = new BaseResult(1,"恭喜您,可用",null,null) ;

    }
}
