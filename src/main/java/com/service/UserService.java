package com.service;

import com.pojo.BaseResult;

/**
 * 作者：涛
 * 时间：2023/5/8 17:42
 * 描述：针对用户的业务接口
 */
public interface UserService {
    /**
     * 检验用户名
     * @param username 前端传过来的用户名
     * @return 返回结果信息
     */
   BaseResult checkUser(String username);
}
