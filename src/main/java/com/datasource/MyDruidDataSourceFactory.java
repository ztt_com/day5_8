package com.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.pooled.PooledDataSourceFactory;

/**
 * 作者：涛
 * 时间：2023/5/6 20:16
 * 描述：自定义数据源工厂--提供德鲁伊的数据源DruidDataSource
 */
public class MyDruidDataSourceFactory extends PooledDataSourceFactory {
    public MyDruidDataSourceFactory(){
        //给数据源初始化
        this.dataSource = new DruidDataSource();//创建德鲁伊数据源
    }
}
