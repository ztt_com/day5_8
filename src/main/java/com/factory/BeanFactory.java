package com.factory;

import com.service.UserService;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.InputStream;

/**
 * 作者：涛
 * 时间：2023/5/9 19:19
 * 描述：定义工厂
 */
public class BeanFactory {

    private BeanFactory(){}//外界不能new
    //对外提供访问方法
    public static Object getBean(String id){

            Object obj = null;
                try {
                    System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                            "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
                    //解析xml文件
                    //使用dom4j--pom文件中引入 核心的dom4j的jar包
                    //创建dom4j解析器 SAXReader
                    SAXReader saxReader = new SAXReader();
                    //读取文件
                    InputStream inputStream = BeanFactory.class.getClassLoader().getResourceAsStream("bean.xml");
                    //bean.xml文档对象Document
                    Document document = saxReader.read(inputStream);
                    //过去到跟标签beans标签对象
                    Element rootElement = document.getRootElement();
                    System.out.println(rootElement);
                    //使用dom4j里面技术---xpath技术:快递定位到某个标签
                    //不分层级选中bena标签--指定id属性的标签
                    //      xpath表达式 "//bean[@id='"+id+"']" //里面id传进去的变量
                    Element element = (Element) document.selectSingleNode("//bean[@id='" + id + "']");//Node节点对象--->指定Element对象
                    //通过bean标签对象获取class属性的内容
                    String className = element.attributeValue("class");
                    System.out.println(className);//接口实现类的完全限定名称

                    //通过反射：创建这个类的实例
                    Class name = Class.forName(className);
                    obj = name.newInstance();
                    return obj;
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                e.printStackTrace();
                } catch (DocumentException e) {
                 e.printStackTrace();
        }
                return null;
    }
    public static void main(String[] args) throws
            DocumentException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        UserService userService = (UserService) BeanFactory.getBean("userService");
        System.out.println(userService) ;
    }

}
