package com.controller;
/**
 * 作者：涛
 * 时间：2023/5/8 16:07
 * 描述：异步校验用户名的后端服务地址
 */

import com.alibaba.fastjson.JSONObject;
import com.factory.BeanFactory;
import com.pojo.BaseResult;
import com.service.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/checkUser")
public class CheckUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //接受参数
        String username = request.getParameter("username") ;
        //调用业务接口
        //UserService userService = new UserServiceImpl() ;
        //自定义工厂，解析bean.xml --- 解耦
        UserService userService = (UserService) BeanFactory.getBean("userService");
        BaseResult baseResult = userService.checkUser(username) ;

        //通过fastjson解析工具解析json
        JSONObject jsonObject = new JSONObject() ;
        String jsonString = jsonObject.toJSONString(baseResult);

        //设置解决响应json的中文乱码
        response.setContentType("application/json;charset=utf-8");
        System.out.println(jsonString);
        //响应
        response.getWriter().write(jsonString);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
