package com.utils;

import com.mapper.UserDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * 作者：涛
 * 时间：2023/5/8 15:33
 * 描述：自定义的Mybatis工具,主要作用创建SqlSession以及通过mybatis实现接口对象的创建(动态代理)
 */
public class MyBatisUtil {

    private MyBatisUtil() {}

    //声明一个静态遍历
    private static SqlSession sqlSession;
    private static SqlSessionFactory sqlSessionFactory;

    //创建一个静态实例
    //每一个线程使用自己的sqlSession，连接数据库进行操作
    private static ThreadLocal<SqlSession> tl = new ThreadLocal<>();
    //静态代码块---获取到SqlSession
    static {
        //1)读取mybatis-config.xml文件
        try {
            System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
                    "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl");
            InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
            //创建SqlSessionFactoryBuilder---它创建工厂对象
            SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder() ;
            sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream) ;
            //创建SqlSession
            sqlSession = sqlSessionFactory.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //2)定义一个获取SqlSession
    public static SqlSession openSession(){
        //从当前线程先获取
        SqlSession sqlSession = tl.get();
        if(sqlSession==null){
            //说明当前正在执行的线程没有SqlSession
            //从连接池获取SqlSession(SqlSessionFactory获取sqlSession)
            sqlSession = sqlSessionFactory.openSession();
            //将当前这个sqlSession绑定到线程上
            tl.set(sqlSession) ;
        }

        return sqlSession;
    }

    //3)提交事务
    public static void commitAndClose(){
        SqlSession sqlSession = openSession();
        sqlSession.commit();
        sqlSession.close() ;
    }
    //4)事务回滚
    public static void rollbackAndClose(){
        SqlSession sqlSession = openSession();
        sqlSession.rollback();
        sqlSession.close();
    }

    //5)定义一个方法:获取任意接口类型的子实现类对象(mybatis动态代理实现)
    //将泛型定义在方法上 -->方法返回值上<T>
    //方法的形式参数:任意java类型的字节码文件对象
    public  static <T extends  Object> T getMapper(Class<T> clazz){
        return sqlSession.getMapper(clazz) ;
    }


    public static void main(String[] args) {
        SqlSession sqlSession = MyBatisUtil.openSession() ;
        System.out.println(sqlSession) ;
        System.out.println("----------------------------------------") ;
        UserDao mapper = MyBatisUtil.getMapper(UserDao.class);
        System.out.println(mapper);
    }

}
