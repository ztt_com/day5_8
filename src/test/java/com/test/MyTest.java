package com.test;

import com.mapper.AdminDao;
import com.mapper.AdminRoleDao;
import com.pojo.Admin;
import com.pojo.AdminRole;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 作者：涛
 * 时间：2023/5/8 21:11
 * 描述：
 */
public class MyTest {
    private AdminDao adminDao;
    private SqlSession sqlSession;
    private AdminRoleDao adminRoleDao;

    @Before//执行单元测试方法之前执行
    public void init() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        //2)创建一个对象SqlSessionFactoryBuilder
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        //3)创建SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(inputStream);//加载流对象 获取到工厂对象
        //4)创建 SqlSession---就是执行对象--底层就是PreparedStatement
        sqlSession = sqlSessionFactory.openSession(false);//自动提交

        adminRoleDao = sqlSession.getMapper(AdminRoleDao.class);
        adminDao = sqlSession.getMapper(AdminDao.class);
    }

    //测试查询所有管理员角色信息
    @Test
    public void testFindAllAdminRole() {
        List<AdminRole> allAdminRole = adminRoleDao.findAllAdminRole();
        System.out.println("管理员角色信息是：");
        if (allAdminRole != null || allAdminRole.size() > 0) {
            for (AdminRole adminRole : allAdminRole) {
                System.out.println(adminRole);
                System.out.println("----------------");
                System.out.println("管理员从属的信息是：");
                System.out.println(adminRole.getAdmin());
            }
        }
    }

    //测试一个管理员查询多个角色
    @Test
    public void testFindAdminRole() {
        List<Admin> admin = adminDao.findAdminRole();
        System.out.println("管理员是：");
        if (admin != null || admin.size() > 0) {
            for (Admin a : admin) {
                System.out.println(a);
                System.out.println("----------------");
                System.out.println("管理员角色从属的信息是：");
                System.out.println(a.getAdminRoles());
            }
        }
    }

    //测试一个角色又包含多个用户
    @Test
    public void testFindAdmin() {
        List<AdminRole> adminRoles = adminRoleDao.findAdmin();
        System.out.println("管理员角色信息是：");
        if (adminRoles != null || adminRoles.size() > 0) {
            for (AdminRole adminRole : adminRoles) {
                System.out.println(adminRole);
                System.out.println("----------------");
                System.out.println("管理员的信息是：");
                System.out.println(adminRole.getAdmin());
            }
        }
    }
}
